#include <stdio.h>
#include <stdlib.h>

#define MAXLEN 255

void markNegativeNumbers(char *in) {
	for (int i = 0; i < MAXLEN && in[i] != '\0'; ++i) {
		if (in[i] == '-') {
			if (i == 0) {
				in[i] = 'u';
			}
			else if (in[i - 1] == '(') {
				in[i] = 'u';
			}
		}
	}
}

double strToDouble(char *in) {
	int delimPosition = -1;
	double result, temp;
	result = temp = 0;
	int i, j;	// j - places after delimeter
	i = j = 0;
	for (i = 0; i < MAXLEN && in[i] != '\0'; ++i) {
		if (in[i] == '.') {
			break;
		}
		result *= 10;
		result += in[i] - '0';
	}
	for (++i; i < MAXLEN && in[i] != '\0'; ++i) {
		temp *= 10;
		temp += in[i] - '0';
		++j;
	}
	for (i = 0; i < j; ++i) {
		temp /= 10;
	}

	return result + temp;
}



int main() {
	char *input;
	input = calloc(MAXLEN, sizeof(char));
	if ((input = gets(input)) != NULL) {		
		printf("%s\n", input);
		markNegativeNumbers(input);
		printf("%s\n", input);
	}

	free(input);

	return 0;
}